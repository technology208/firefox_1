#!/bin/sh
version=$1
wget -r -np -nH -R index.html https://ftp.mozilla.org/pub/firefox/releases/$version/linux-x86_64/xpi/
mkdir firefox-langpacks
find -name *.xpi | mv `xargs` firefox-langpacks/
tar -cJf firefox-langpacks-$version.tar.xz firefox-langpacks
rm -rf pub firefox-langpacks
